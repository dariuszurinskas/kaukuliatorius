﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kaukuliatorius
{
    public partial class Form1 : Form
    {
        public string X;
        public string Operacija;

        public Form1()
        {
            InitializeComponent();
        }
  



        public void AtspausdinkSkaiciu(string skaicius)
        {
            if (Ekranas.Text.Length < 8)

            {
                Ekranas.Text = Ekranas.Text + skaicius;
            }
        }
        
        private void button17_Click(object sender, EventArgs e)
        {
            AtspausdinkSkaiciu("7");
        }

        private void button14_Click(object sender, EventArgs e)

        {
            if (Ekranas.Text == "0")
            {
               return;
            }

            AtspausdinkSkaiciu("0");
        }
         
        private void vienas_Click(object sender, EventArgs e)
        {
            AtspausdinkSkaiciu("1");
        }

        private void du_Click(object sender, EventArgs e)
        {
            AtspausdinkSkaiciu("2");
        }

        private void trys_Click(object sender, EventArgs e)
        {
            AtspausdinkSkaiciu("3");
        }

        private void keturi_Click(object sender, EventArgs e)
        {
            AtspausdinkSkaiciu("4");
        }

        private void penki_Click(object sender, EventArgs e)
        {
            AtspausdinkSkaiciu("5");
        }

        private void sesi_Click(object sender, EventArgs e)
        {
            AtspausdinkSkaiciu("6");
        }

        private void astuoni_Click(object sender, EventArgs e)
        {
            AtspausdinkSkaiciu("8");
        }

        private void devyni_Click(object sender, EventArgs e)
        {
            AtspausdinkSkaiciu("9");
        }

        private void kablelis_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "")
            {
                Ekranas.Text = Ekranas.Text +"0";
            }

            if (!Ekranas.Text.Contains(","))
            {
                Ekranas.Text = Ekranas.Text + ",";
            }
           
        }

        private void plius_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            Operacija = "+";
        }

        private void minus_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            Operacija = "-";
        }

        private void daugyba_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            Operacija = "*";
        }

        private void dalyba_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            Operacija = "/";
        }

        private void lygu_Click(object sender, EventArgs e)
        {

            float x = Convert.ToSingle(X);
           
            if (Ekranas.Text == "" || X == "")
            {
                return;
            }
            float y = Convert.ToSingle(Ekranas.Text);

            
            float rezultatas = 0;

            if (Operacija == "+")
            {
                rezultatas = x + y;
            }
            else if (Operacija == "-")
            {
                rezultatas = x - y;
            }
            else if (Operacija == "*")
            {
                rezultatas = x * y;
            }
            else if (Operacija == "/")
            {
                rezultatas = x / y;
            }
            

            Ekranas.Text = rezultatas.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Trinti_Click(object sender, EventArgs e)
        {
            Ekranas.Clear();
        }

        private void pliusasminusas_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "")
            {
                return;
            }
            float y = Convert.ToSingle(Ekranas.Text);
           
            Ekranas.Text = (y * -1).ToString();
        }

        private void trinaViena_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "") { return; }

           Ekranas.Text = Ekranas.Text.Remove(Ekranas.Text.Length - 1);
        }

        private void saknis_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "") { return; }
            float y = Convert.ToSingle(Ekranas.Text);

            Ekranas.Text = Math.Sqrt(y).ToString();
        }

        private void Ekranas_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
