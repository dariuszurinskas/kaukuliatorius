﻿namespace Kaukuliatorius
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.minus = new System.Windows.Forms.Button();
            this.penki = new System.Windows.Forms.Button();
            this.sesi = new System.Windows.Forms.Button();
            this.dalyba = new System.Windows.Forms.Button();
            this.daugyba = new System.Windows.Forms.Button();
            this.Trinti = new System.Windows.Forms.Button();
            this.keturi = new System.Windows.Forms.Button();
            this.lygu = new System.Windows.Forms.Button();
            this.trys = new System.Windows.Forms.Button();
            this.du = new System.Windows.Forms.Button();
            this.vienas = new System.Windows.Forms.Button();
            this.plius = new System.Windows.Forms.Button();
            this.kablelis = new System.Windows.Forms.Button();
            this.Nulis = new System.Windows.Forms.Button();
            this.devyni = new System.Windows.Forms.Button();
            this.astuoni = new System.Windows.Forms.Button();
            this.septyni = new System.Windows.Forms.Button();
            this.pliusasminusas = new System.Windows.Forms.Button();
            this.trinaViena = new System.Windows.Forms.Button();
            this.saknis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ekranas
            // 
            this.Ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Ekranas.Location = new System.Drawing.Point(21, 28);
            this.Ekranas.Multiline = true;
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(257, 41);
            this.Ekranas.TabIndex = 0;
            this.Ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Ekranas.TextChanged += new System.EventHandler(this.Ekranas_TextChanged);
            // 
            // minus
            // 
            this.minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.minus.Location = new System.Drawing.Point(182, 212);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(45, 33);
            this.minus.TabIndex = 1;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // penki
            // 
            this.penki.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.penki.Location = new System.Drawing.Point(72, 173);
            this.penki.Name = "penki";
            this.penki.Size = new System.Drawing.Size(45, 33);
            this.penki.TabIndex = 2;
            this.penki.Text = "5";
            this.penki.UseVisualStyleBackColor = true;
            this.penki.Click += new System.EventHandler(this.penki_Click);
            // 
            // sesi
            // 
            this.sesi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.sesi.Location = new System.Drawing.Point(123, 173);
            this.sesi.Name = "sesi";
            this.sesi.Size = new System.Drawing.Size(45, 33);
            this.sesi.TabIndex = 3;
            this.sesi.Text = "6";
            this.sesi.UseVisualStyleBackColor = true;
            this.sesi.Click += new System.EventHandler(this.sesi_Click);
            // 
            // dalyba
            // 
            this.dalyba.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.dalyba.Location = new System.Drawing.Point(182, 134);
            this.dalyba.Name = "dalyba";
            this.dalyba.Size = new System.Drawing.Size(45, 33);
            this.dalyba.TabIndex = 4;
            this.dalyba.Text = "/";
            this.dalyba.UseVisualStyleBackColor = true;
            this.dalyba.Click += new System.EventHandler(this.dalyba_Click);
            // 
            // daugyba
            // 
            this.daugyba.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.daugyba.Location = new System.Drawing.Point(182, 173);
            this.daugyba.Name = "daugyba";
            this.daugyba.Size = new System.Drawing.Size(45, 33);
            this.daugyba.TabIndex = 5;
            this.daugyba.Text = "*";
            this.daugyba.UseVisualStyleBackColor = true;
            this.daugyba.Click += new System.EventHandler(this.daugyba_Click);
            // 
            // Trinti
            // 
            this.Trinti.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Trinti.Location = new System.Drawing.Point(182, 95);
            this.Trinti.Name = "Trinti";
            this.Trinti.Size = new System.Drawing.Size(96, 33);
            this.Trinti.TabIndex = 6;
            this.Trinti.Text = "C";
            this.Trinti.UseVisualStyleBackColor = true;
            this.Trinti.Click += new System.EventHandler(this.Trinti_Click);
            // 
            // keturi
            // 
            this.keturi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.keturi.Location = new System.Drawing.Point(21, 173);
            this.keturi.Name = "keturi";
            this.keturi.Size = new System.Drawing.Size(45, 33);
            this.keturi.TabIndex = 7;
            this.keturi.Text = "4";
            this.keturi.UseVisualStyleBackColor = true;
            this.keturi.Click += new System.EventHandler(this.keturi_Click);
            // 
            // lygu
            // 
            this.lygu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lygu.Location = new System.Drawing.Point(233, 212);
            this.lygu.Name = "lygu";
            this.lygu.Size = new System.Drawing.Size(45, 72);
            this.lygu.TabIndex = 8;
            this.lygu.Text = "=";
            this.lygu.UseVisualStyleBackColor = true;
            this.lygu.Click += new System.EventHandler(this.lygu_Click);
            // 
            // trys
            // 
            this.trys.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.trys.Location = new System.Drawing.Point(123, 212);
            this.trys.Name = "trys";
            this.trys.Size = new System.Drawing.Size(45, 33);
            this.trys.TabIndex = 9;
            this.trys.Text = "3";
            this.trys.UseVisualStyleBackColor = true;
            this.trys.Click += new System.EventHandler(this.trys_Click);
            // 
            // du
            // 
            this.du.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.du.Location = new System.Drawing.Point(72, 212);
            this.du.Name = "du";
            this.du.Size = new System.Drawing.Size(45, 33);
            this.du.TabIndex = 10;
            this.du.Text = "2";
            this.du.UseVisualStyleBackColor = true;
            this.du.Click += new System.EventHandler(this.du_Click);
            // 
            // vienas
            // 
            this.vienas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.vienas.Location = new System.Drawing.Point(21, 212);
            this.vienas.Name = "vienas";
            this.vienas.Size = new System.Drawing.Size(45, 33);
            this.vienas.TabIndex = 11;
            this.vienas.Text = "1";
            this.vienas.UseVisualStyleBackColor = true;
            this.vienas.Click += new System.EventHandler(this.vienas_Click);
            // 
            // plius
            // 
            this.plius.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.plius.Location = new System.Drawing.Point(182, 251);
            this.plius.Name = "plius";
            this.plius.Size = new System.Drawing.Size(45, 33);
            this.plius.TabIndex = 12;
            this.plius.Text = "+";
            this.plius.UseVisualStyleBackColor = true;
            this.plius.Click += new System.EventHandler(this.plius_Click);
            // 
            // kablelis
            // 
            this.kablelis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.kablelis.Location = new System.Drawing.Point(123, 251);
            this.kablelis.Name = "kablelis";
            this.kablelis.Size = new System.Drawing.Size(45, 33);
            this.kablelis.TabIndex = 13;
            this.kablelis.Text = ",";
            this.kablelis.UseVisualStyleBackColor = true;
            this.kablelis.Click += new System.EventHandler(this.kablelis_Click);
            // 
            // Nulis
            // 
            this.Nulis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Nulis.Location = new System.Drawing.Point(21, 251);
            this.Nulis.Name = "Nulis";
            this.Nulis.Size = new System.Drawing.Size(96, 33);
            this.Nulis.TabIndex = 14;
            this.Nulis.Text = "0";
            this.Nulis.UseVisualStyleBackColor = true;
            this.Nulis.Click += new System.EventHandler(this.button14_Click);
            // 
            // devyni
            // 
            this.devyni.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.devyni.Location = new System.Drawing.Point(123, 134);
            this.devyni.Name = "devyni";
            this.devyni.Size = new System.Drawing.Size(45, 33);
            this.devyni.TabIndex = 15;
            this.devyni.Text = "9";
            this.devyni.UseVisualStyleBackColor = true;
            this.devyni.Click += new System.EventHandler(this.devyni_Click);
            // 
            // astuoni
            // 
            this.astuoni.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.astuoni.Location = new System.Drawing.Point(72, 134);
            this.astuoni.Name = "astuoni";
            this.astuoni.Size = new System.Drawing.Size(45, 33);
            this.astuoni.TabIndex = 16;
            this.astuoni.Text = "8";
            this.astuoni.UseVisualStyleBackColor = true;
            this.astuoni.Click += new System.EventHandler(this.astuoni_Click);
            // 
            // septyni
            // 
            this.septyni.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.septyni.Location = new System.Drawing.Point(21, 134);
            this.septyni.Name = "septyni";
            this.septyni.Size = new System.Drawing.Size(45, 33);
            this.septyni.TabIndex = 17;
            this.septyni.Text = "7";
            this.septyni.UseVisualStyleBackColor = true;
            this.septyni.Click += new System.EventHandler(this.button17_Click);
            // 
            // pliusasminusas
            // 
            this.pliusasminusas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.pliusasminusas.Location = new System.Drawing.Point(233, 134);
            this.pliusasminusas.Name = "pliusasminusas";
            this.pliusasminusas.Size = new System.Drawing.Size(45, 33);
            this.pliusasminusas.TabIndex = 18;
            this.pliusasminusas.Text = "+/-";
            this.pliusasminusas.UseVisualStyleBackColor = true;
            this.pliusasminusas.Click += new System.EventHandler(this.pliusasminusas_Click);
            // 
            // trinaViena
            // 
            this.trinaViena.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.trinaViena.Location = new System.Drawing.Point(21, 95);
            this.trinaViena.Name = "trinaViena";
            this.trinaViena.Size = new System.Drawing.Size(147, 33);
            this.trinaViena.TabIndex = 19;
            this.trinaViena.Text = "Back";
            this.trinaViena.UseVisualStyleBackColor = true;
            this.trinaViena.Click += new System.EventHandler(this.trinaViena_Click);
            // 
            // saknis
            // 
            this.saknis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.saknis.Location = new System.Drawing.Point(233, 173);
            this.saknis.Name = "saknis";
            this.saknis.Size = new System.Drawing.Size(45, 33);
            this.saknis.TabIndex = 20;
            this.saknis.Text = "√";
            this.saknis.UseVisualStyleBackColor = true;
            this.saknis.Click += new System.EventHandler(this.saknis_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 310);
            this.Controls.Add(this.saknis);
            this.Controls.Add(this.trinaViena);
            this.Controls.Add(this.pliusasminusas);
            this.Controls.Add(this.septyni);
            this.Controls.Add(this.astuoni);
            this.Controls.Add(this.devyni);
            this.Controls.Add(this.Nulis);
            this.Controls.Add(this.kablelis);
            this.Controls.Add(this.plius);
            this.Controls.Add(this.vienas);
            this.Controls.Add(this.du);
            this.Controls.Add(this.trys);
            this.Controls.Add(this.lygu);
            this.Controls.Add(this.keturi);
            this.Controls.Add(this.Trinti);
            this.Controls.Add(this.daugyba);
            this.Controls.Add(this.dalyba);
            this.Controls.Add(this.sesi);
            this.Controls.Add(this.penki);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.Ekranas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Skaiciuotuvas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button penki;
        private System.Windows.Forms.Button sesi;
        private System.Windows.Forms.Button dalyba;
        private System.Windows.Forms.Button daugyba;
        private System.Windows.Forms.Button Trinti;
        private System.Windows.Forms.Button keturi;
        private System.Windows.Forms.Button lygu;
        private System.Windows.Forms.Button trys;
        private System.Windows.Forms.Button du;
        private System.Windows.Forms.Button vienas;
        private System.Windows.Forms.Button plius;
        private System.Windows.Forms.Button kablelis;
        private System.Windows.Forms.Button Nulis;
        private System.Windows.Forms.Button devyni;
        private System.Windows.Forms.Button astuoni;
        private System.Windows.Forms.Button septyni;
        private System.Windows.Forms.Button pliusasminusas;
        private System.Windows.Forms.Button trinaViena;
        private System.Windows.Forms.Button saknis;
    }
}

